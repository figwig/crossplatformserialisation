
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "Bob2.h"
#include "OutputMemoryBitStream.h"



int main(void)
{
	OutputMemoryBitStream stream;

	Bob* b = new Bob(1.5f, 1.56f, 8.5f, 0,2,42);

	b->Write(stream);

	delete b;

	int byteLength = stream.GetByteLength();

	int copyLen =  byteLength;
	char* copyBuff = new char[copyLen];
	// Copy over the buffer
	memcpy(copyBuff, stream.GetBufferPtr(), copyLen);
	// Create a new memory stream
	

	InputMemoryBitStream in = InputMemoryBitStream(copyBuff, byteLength * 8);

	//delete copyBuff;

	Bob* resurectedBob = new Bob();
	resurectedBob->Read(in);

	delete resurectedBob;
}

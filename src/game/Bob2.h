#pragma once
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"
class Bob
{
public:
	Bob(float i, float j, float k, int x, int y, int z) : i(i), j(j), k(k), x(x), y(x), z(z)
	{

	}

	Bob()
	{

	}

	float getI() { return i; }
	float getJ() { return j; }
	float getK() { return k; }

	int getX() { return x; }
	int getY() { return y; }
	int getZ() { return z; }
	void Write(OutputMemoryBitStream& inOutputStream) 
	{
		inOutputStream.Write(i);
		inOutputStream.Write(j);
		inOutputStream.Write(k);

		inOutputStream.Write(x);
		inOutputStream.Write(y);
		inOutputStream.Write(z);
	}

	void Read(InputMemoryBitStream& inInputStream) 
	{
		float iTemp = 0.0f;
		float jTemp = 0.0f;
		float kTemp = 0.0f;

		inInputStream.Read(iTemp);
		inInputStream.Read(jTemp);
		inInputStream.Read(kTemp);


		int xTemp = 0;
		int yTemp = 0;
		int zTemp = 0;
		inInputStream.Read(xTemp);
		inInputStream.Read(yTemp);
		inInputStream.Read(zTemp);

		i = iTemp;
		j = jTemp;
		k = kTemp;

		x = xTemp;
		y = yTemp;
		z = zTemp;
	}

private:

	bool isBobAmazing = true;

	float i = 0.0f;
	float j = 0.0f;
	float k = 0.0f;

	int x = 0;
	int y = 0;
	int z = 0;


};
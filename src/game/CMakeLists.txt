

# Add executable called "helloDemo" that is built from the source files
# "demo.cxx" and "demo_b.cxx". The extensions are automatically found.

add_executable (Game main.cpp)

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
#target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)

# Link the executable to the Hello library. Since the Hello library has
# public include directories we will use those link directories when building

#Game
target_link_libraries (Game LINK_PUBLIC strings)
target_link_libraries (Game LINK_PUBLIC maths)
#target_link_libraries (Game LINK_PUBLIC networking)
target_link_libraries (Game LINK_PUBLIC serialisation)
target_link_libraries (Game LINK_PUBLIC engine)


## For the tutorial!
add_executable (Client client.cpp)

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
#target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)

# Link the executable to the Hello library. Since the Hello library has
# public include directories we will use those link directories when building

#Game
target_link_libraries (Client LINK_PUBLIC strings)
target_link_libraries (Client LINK_PUBLIC maths)
#target_link_libraries (Game LINK_PUBLIC networking)
target_link_libraries (Client LINK_PUBLIC serialisation)
target_link_libraries (Client LINK_PUBLIC engine)
